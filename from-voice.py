import enum
import os
import re
from datetime import datetime

from bs4 import BeautifulSoup


class MessageType(enum.Enum):
    Received = 1
    Sent = 2


class Message:
    def __init__(self, address: int, date: datetime, msg_type: MessageType, body: str, valid=True):
        self.address = address
        self.date = date
        self.msg_type = msg_type
        self.body = body
        self.valid = valid

    def get_xml(self) -> str:
        return f"""<sms protocol="0" address="+{self.address}" date="{self.date_to_timestamp(self.date)}" type="{self.msg_type.value}" subject="null" body="{self.body}" toa="null" sc_toa="null" read="1" status="-1" locked="0" sub_id="1" />"""

    @staticmethod
    def date_to_timestamp(date: datetime) -> int:
        return int(date.timestamp() * 1000)

    def is_valid(self):
        if not self.valid:
            # print('Declared not valid')
            return False
        if not isinstance(self.address, int):
            print('Bad number', self.address)
            return False
        if not isinstance(self.date, datetime):
            print('Bad timestamp', self.date)
            return False
        return True

    def __str__(self):
        return f'{self.address}: {self.body}'


class MMS(Message):
    def __init__(self, *args, img_src: str, **kwargs):
        super().__init__(*args, **kwargs)
        self.img_src = img_src


class XMLFileMaker:
    def __init__(self, output_filepath: str):
        self.filepath = output_filepath
        self.messages = []

    def add_messages(self, messages):
        for message in list(messages):
            self._add_message(message)

    def _add_message(self, message):
        assert isinstance(message, Message)
        if not message.is_valid():
            return
        self.messages.append(message)

    def get_xml_str(self):
        xml_lines = '\n'.join([message.get_xml() for message in self.messages])
        return f"""<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<!--File created for use with SMS Backup & Restore v10.05.612 on {datetime.today().ctime()}-->
<smses count="{len(self.messages)}" backup_set="4fd351a5-f4b7-43bf-b0ea-8ebd90698a23" backup_date="{Message.date_to_timestamp(datetime.now())}">
{xml_lines}
</smses>"""

    def write(self):
        with open(self.filepath, 'w') as f:
            f.writelines(self.get_xml_str())


class HTMLParser:
    def __init__(self, filepath: str, my_number: int):
        self.filepath = filepath
        self.soup = BeautifulSoup(open(filepath))
        self.my_number = my_number
        self.number = self.get_correspondent_number()
        self.messages = []

    def get_correspondent_number(self):
        for cite in self.soup.find_all('cite'):
            number = cite.find('a').get('href')
            number = ''.join(re.findall(r'\d+', number))
            try:
                number = int(number)
            except ValueError:
                pass
            if number and number != self.my_number:
                return number
        filename = os.path.basename(self.filepath)
        if filename.startswith('+1'):
            number = filename[1:11]
            assert len(number) == 10
            try:
                return int(number)
            except ValueError:
                print('Error processing', filename)
                return ''
        print('Only number found is my own in file', self.filepath)
        return ''

    @staticmethod
    def is_mms(div):
        if div.find('img') is not None:
            return True
        if div.find('a'):
            cls = div.find('a').get('class')
            if 'video' in cls:
                # TODO: We can't really handle video MMS
                return True
        return False

    def parse_divs(self):
        for div in self.soup.find_all('div')[1:-1]:
            if self.is_mms(div):
                msg = self.parse_mms(div)
            else:
                msg = self.parse_sms(div)
            if msg.is_valid():
                self.messages.append(msg)

    def parse_sms(self, msg):
        try:
            text = msg.find('q')
            if text:
                text = text.getText()
            else:
                text = ''

            time = msg.find('abbr').get('title')
            time = datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%f%z')

            number = msg.find('cite').find('a').get('href')
            number = ''.join(re.findall(r'\d+', number))
            number = int(number)

            if number == self.my_number:
                msg_type = MessageType.Sent
            else:
                msg_type = MessageType.Received
            return Message(address=self.number, date=time, msg_type=msg_type, body=text)
        except (AttributeError, ValueError) as e:
            print('Bad SMS because', e)
            return Message(address=None, date=None, msg_type=None, body=None, valid=False)

    def parse_mms(self, msg):
        try:
            # Impossible to tell from the data, unfortunately, so we assume it was received
            msg_type = MessageType.Received
            img_src = msg.find('img').get('src')
            # TODO: Fix timestamp
            return MMS(address=self.number, date=None, msg_type=msg_type, body=None, img_src=img_src)
        except (AttributeError, ValueError) as e:
            print('Bad MMS because', e)
            return MMS(address=self.number, date=None, msg_type=None, body=None, img_src=None, valid=False)


if __name__ == '__main__':
    base_path = './Voice/Calls'
    paths = [os.path.join(base_path, path) for path in os.listdir(base_path) if path.endswith('.html') and 'Text' in path]
    my_number = 18017170681

    xml_maker = XMLFileMaker('test_xml.xml')
    for path in paths:
        parser = HTMLParser(path, my_number)
        parser.parse_divs()
        xml_maker.add_messages(parser.messages)
    xml_maker.write()
